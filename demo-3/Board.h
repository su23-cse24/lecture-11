#ifndef BOARD_H
#define BOARD_H

#include <iostream>

enum SquareState {EMPTY, PLAYER_1, PLAYER_2};

struct Board {
    int size;
    SquareState** grid;

    // default constructor
    Board() {
        std::cout << "default constructor" << std::endl;
        size = 3;
        initGrid();
    }

    // overloaded constructor
    Board(int size) {
        std::cout << "overloaded constructor" << std::endl;
        this->size = size;
        initGrid();
    }

    void initGrid() {
        grid = new SquareState*[size];

        for (int i = 0; i < size; i++) {
            grid[i] = new SquareState[size];
        }

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                grid[i][j] = EMPTY;
            }
        }
    }


    ~Board() {
        std::cout << "destructor" << std::endl;
        for (int i = 0; i < size; i++) {
            delete[] grid[i];
        }
        delete[] grid;
    }
};

std::ostream& operator<<(std::ostream& os, const Board& board) {
    for (int i = 0; i < board.size; i++) {
        for (int j = 0; j < board.size; j++) {
            if (board.grid[i][j] == EMPTY) {
                os << "-";
            } else if (board.grid[i][j] == PLAYER_1) {
                os << "X";
            } else if (board.grid[i][j] == PLAYER_2) {
                os << "O";
            }
            os << " ";
        }
        os << std::endl;
    }
    return os;
}

#endif