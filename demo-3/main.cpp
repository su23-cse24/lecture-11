#include<iostream>
#include "Board.h"
using namespace std;

int main() {

    Board board;
    board.grid[0][0] = PLAYER_1;

    Board copy = board;

    board.grid[1][1] = PLAYER_2;

    cout << "board: " << (long) board.grid << endl;
    cout << board;

    cout << "copy: " << (long) copy.grid << endl;
    cout << copy;

    return 0;
}