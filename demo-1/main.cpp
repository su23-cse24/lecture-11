#include<iostream>
using namespace std;

struct Car {
    int year;
    string make;
    string model;

    // default constructor
    Car() {
        // cout << "default constructor" << endl;
        year = 2023;
        make = "Toyota";
        model = "Camry";
    }

    // overloaded constructor
    Car(int year, string make, string model) {
        // cout << "overloaded constructor" << endl;
        this->year = year;
        this->make = make;
        this->model = model;
    }
};

// overloading the shift left (<<) operator
ostream& operator<<(ostream& os, const Car& car) {
    os << "Car: " << car.year << " " << car.make << " " << car.model << endl;
    return os;
}

int main() {
    Car car1;
    Car car2(2023, "Honda", "Accord");

    cout << car1 << car2;

    return 0;
}